from tkinter import *
import datetime
import time
from playsound import playsound



def Alarm(time_of_alarm):
    while True:
        time.sleep(1)
        actual_time = datetime.datetime.now()
        current_time = actual_time.strftime("%H:%M:%S")
        current_date = actual_time.strftime("%d/%m/%Y") #?
        message = "Heure actuelle:" + str(current_time)
        print (message)
        if current_time == time_of_alarm:
           playsound("alarm.mp3") 
           break

def get_time_of_alarm():
    alarm_set_time = f"{hour.get()}:{min.get()}:{sec.get()}"
    Alarm(alarm_set_time)

window = Tk()
window.geometry("400x210") 
window.title("Alarm") 
window.resizable(width=False,height=False)
 

addTime = Label(window,text = "Hour    Min     Sec",font=60,fg="black").place(x = 220)
choose_time = Label(text="Choisissez l'heure de l'alarme") 
choose_time.place(x=8, y=20) 


hour = StringVar()
min = StringVar()
sec = StringVar()

hourTime = IntVar() 
Spinbox(width=4, from_=0, to=23, state="readonly", textvariable=hour, 
wrap=True).place(x=210, y=20) 
 
minTime = IntVar() 
Spinbox(width=4, from_=0, to=59, state="readonly", textvariable=min, 
wrap=True).place(x=270, y=20)

secTime = IntVar() 
Spinbox(width=4, from_=0, to=59, state="readonly", textvariable=sec, 
wrap=True).place(x=330, y=20) 
 
submit = Button(window,text = "Mettre votre alarme",fg="Black",bg="#D4AC0D",width = 15,command = get_time_of_alarm,font=(20)).place(x =100,y=80)

exit = Button(text="Quitter",width=10, command=window.destroy) 
exit.place(x=20, y=170)
 
 
window.mainloop()


